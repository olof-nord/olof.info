---
title: "Apt Cache"
description: "How to get available apt versions"
date: 2022-11-02T10:21:20+01:00
draft: false
---

`apt`, the commandline package manager for Debian, Ubuntu and derivatives, has a nice way to list available versions of
packages to install.

Here the output from Debian bookworm/sid, x86_64:

```shell
apt-cache policy git
git:
  Installed: (none)
  Candidate: 1:2.38.1-1
  Version table:
     1:2.38.1-1 500
        500 http://deb.debian.org/debian sid/main amd64 Packages
```

Here the output from Debian bookworm/sid, riscv64:

```shell
apt-cache policy git
git:
  Installed: (none)
  Candidate: 1:2.37.2-1
  Version table:
     1:2.37.2-1 500
        500 http://deb.debian.org/debian-ports sid/main riscv64 Packages
```

This can then later be used to specify exactly which version to install:

```shell
sudo apt install -y git=1:2.38.1-1
```
