---
title: "Bluetooth headset with Linux"
description: "Instructions and information regarding using and configuring a bluetooth headset with Linux"
date: 2021-02-07T19:52:40+01:00
draft: false
---

Since a while I am a very happy user of a Bose QuietComfort 35 II bluetooth headset.

It has a wearing comfort which is beating anything which I have experienced before.
Admittedly I have not used too many headsets previously, and for a long time I thought there could be nothing better.
(I recently found out there could: the headset
[does not](https://community.bose.com/t5/Headphones-Archive/QC35ii-LDAC-aptX/td-p/89402) support the bluetooth `aptX`
codec - more to that below)

### How to sound with Linux?

There exists a few different audio server options: alsa, jack, pulseaudio and more recently, pipewire.

Having tried out the two latter, I got the best results with pipewire.

It is the most recent project of the ones listed (I believe), and setting it up is straight-forward.

For as long as I knew, pulseaudio was the go-to option and practically the default option. That might be about to
change as the pipewire project matures and gets used by more and more distributions.

### Avoid GNOME Sound Settings

Note that at least for me, controlling the sound  through Settings -> Sound with GNOME looks like it works, but turns
out it sets the configuration incorrectly and does more harm than help. Specifically, using it to activate the A2DP
profile (see below) effectively renders the headset useless.

The solution I have found is using `pavucontrol` (PulseAudio Volume Control), it works without any unintended
side effects and also offers a GUI.

### PipeWire

Pipewire is recently included in the extra Arch Linux repository, and can be installed without any AUR helper.
That also indicates its maturity and acceptance by the community.

Install:

`pacman -S pipewire pipewire-pulse`

Start and set to autostart:

`systemctl --user enable --now pipewire pipewire-pulse`

### Bluetooth profiles

Now, in a perfect world installing it would've been it, but then there would be no need to write a blog post, right?

Bluetooth audio has some different profiles, each one with a specific purpose.

![screenshot of pavucontrol profiles for Bose QC 35 II](/images/posts/pavucontrol-config.png)

#### A2DP

This is the profile to use when *listening* to music. There are some different codecs available, SBC, AAC and aptX.
The sound quality is really high (compared to HSP), but it does not have any microphone support.
This is because A2DP is *unidirectional*, meaning that input together with output is a no-go.

#### HSP/FDP

Would one like to conduct a video call (as one seems to do a lot now during corona times), then another bluetooth
profile is required.

With the HSP/FDP the headset microphone can be detected as an input source and is consequently possible to use.

The output sound quality of this profile is noticeably lower than A2DP though.

Luckily with PipeWire, switching between the profiles is easy and takes effect directly.

### Bluetooth codecs

Here is an incomplete list of bluetooth codecs. Note that this requires both client and server support: this must be
supported by the headset itself.

* [SBC](https://en.wikipedia.org/wiki/SBC_(codec)): The standard bluetooth codec.
* [aptX](https://en.wikipedia.org/wiki/AptX): This is the newest option, which promises better quality than SBC.
* AAC: For Apple devices
