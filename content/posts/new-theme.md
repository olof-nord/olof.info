---
title: "New site theme"
description: "Description and background to the new site theme"
date: 2020-11-01T20:32:46+01:00
draft: false
---

I decided to leave my old theme behind and switch the focus of the website a bit.

My previous theme, [osprey](https://github.com/tomanistor/osprey), was more aimed for a portfolio, but I felt it made
the site too business-focused.

## Hello hugo-ficurinia

The current theme, [hugo-ficurinia](https://gitlab.com/gabmus/hugo-ficurinia), is developed by
[gabmus](https://gabmus.org/).

Gambus is a software engineer associated with the GNOME project, and they sure know how to design things.

## New theme, new tools

Switching theme also made me discover a [Matomo](https://www.innocraft.cloud) alternative, Plausible.
Both are privacy-focused tools to gather and visualise user interactions to a website.

Apart from [Plausible](https://plausible.io) coming pre-configured with the new theme, it is also 1/4 of the price.
The lowest tier of Matomo costs ca. 20€ a month, whereas Plausible charges 5€.

In addition, the new ficurinia theme supports comments, something which I have been looking for since a while.
The comments are managed by [Commento](https://commento.io), a privacy-focused chat widget.

I must say that the [Hugo](https://gohugo.io) framework have really paid off in this regard, as the clean separation of
design and content makes changing the theme an absolute breeze.

Oh, and where did the portfolio go?
It is still [available](/pages/portfolio), however now no longer on the landing page.
