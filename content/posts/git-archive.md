---
title: "Git Archive"
description: "Fetch a git archive"
date: 2021-03-28T13:19:55+02:00
draft: false
---

I only recently realised that it is possible to fetch a complete copy of a git repository through wget/curl only.

If only the content of the repository is important, this is useful, and is also commonly used. One such usage is an
Arch Linux User Repository (AUR) package.

Here an example with GitHub, the URL is quite self-explanatory. As last argument either a branch, or a tag can be used.

`wget "https://github.com/nanu-c/axolotl/archive/v0.9.9.tar.gz"`

Here is an example with GitLab, where the syntax is a bit more verbose. The number used after "projects" is the
Project ID, which can be found just below the repository name listed in the GitLab UI.

`wget "https://gitlab.com/api/v4/projects/22468999/repository/archive?ref=master" -O olof.info-master.tar.gz`
