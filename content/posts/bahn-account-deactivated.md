---
title: "Deutsche Bahn account deactivated"
date: 2022-10-31T16:30:06+01:00
draft: false
description: "How Deutsche Bahn deactivated my account and did nothing about it"
---

### Validation

On the 30th of September 2022, when logging into my [Deutsche Bahn](https://www.bahn.de) account, I was asked to verify
my email address.

I enter my email, and I receive an email with subject "Verify email" from <no_reply@bahn.de> with a single button:
Activate your account. (yes, I do not use dark mode for my email, warning)

![screenshot of bahn.de verify email](/images/posts/bahn.de-verify-email.png)

My bahn.de account is 6-7 years old, and for some reason this confirmation was needed now.

### Disabled

Following me verifying the email address, once I submit my username and password,
I am facing another message (and this is the reason why I am writing this post):

> Account is disabled, contact your administrator.

Note the red text below the Password field.

![screenshot of bahn.de login form with a disabled account message](/images/posts/bahn.de-account-deactivated.png)

Now, leaving aside the question of why the hCaptcha checkbox is designed with a different width and background (?),
this got me asking: why is my account disabled, and most importantly, how can I contact the administrator?

As one can see on the screenshot, there is no link or info provided, just the red text.

In a sub-sub page of bahn.de, I find the email <kundendialog@bahn.de> which I use to send an email on the 2nd of October
, mentioning that my account is deactivated and asking what this means to my bonus points (like air miles), my BahnCard
ticket discount subscription, and if they could let me know why the account is disabled.

On the 14th of October (12 days after my question), I receive an email informing me that when logging in, I need to
ensure my username and password is entered correctly. Great advice, that really took two working weeks, really?

I use a password manager for my password and do not type them, but they are automatically entered.
For most of my passwords, I don't even know what they are!

Secondly, the error message shown is different when using an incorrect user or password:

> Invalid username or password.

Again, note the red text below the Password field.

![screenshot of bahn.de login form with an invalid username or password message](/images/posts/bahn.de-invalid.png)

### Reactivation/Game over

I follow up the ~~almost~~ insulting advice a few weeks later, once I could stomach the incompetence and sad state of
tech in Germany by showing the two error messages, and highlighting the differences (spoiler: invalid vs disabled).

On my second email I get a reply within 24h (here I cannot complain), which tells me to contact the Online-Service per
_phone_. (for the curious: +49 30 2970). Per phone!

I take another deep breath, and after some waiting in queues and describing the issue to three different employees,
I am told that there is _nothing they can do_, I should just register for a new account.

That is something worth reflecting on a minute or two.

The old account is permanently dead, it is disabled and there is nothing neither they, nor I can do about it.
When I asked why, the answer was "because of data protection (Datenschutz)".

### Second account

I am now the owner of TWO Deutsche Bahn accounts: one which I can use to log in with, and one which is disabled.
One from 2022, and one from 2015. I could re-register everything, even register with the same email as I tried to use
for the first account.

I like to travel with trains. I have bought all my train tickets with Deutsche Bahn through their website.
I have nothing against Deutsche Bahn as a company, but this is just embarrassingly bad.

Two takeaways from this: if this happens to you, call the support on +49 30 2970 and do not send these highly modernized
digital letters some call email. Or, skip the whole farce and just create a new user directly.

### Final thoughts

When registering for the second account, I tried to re-use my password from the first account, and I realised that this
is a password which is not randomized and actually quite simple: it does not include an upper case letter, and it also
does not include a special character. The password for the new account required this: perhaps there was a migration of
my account from without email to with, which forced the account to be disabled as the password was not strong enough.

Issue is, that I have unsuccessfully chosen to reset the password of my first account - however I do not receive any
email. Perhaps the first account never had any associated email? When on the phone to support, I asked if there was any
chance of resetting the password per letter (yes, letter): but that was something they did not have the privileges to do.
