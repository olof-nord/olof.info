---
title: "Adjust AUR source"
description: "Instructions and information how to adjust an AUR source"
date: 2021-11-25T21:51:27+01:00
draft: false
---

Today I want to share how to adjust an AUR PKGBUILD script to point to a different source, for example when a fork of
the original project is needed.

AUR is the [Arch User Repository](https://en.wikipedia.org/wiki/Arch_Linux#Arch_User_Repository_(AUR)), a community
sourced list of packages to install - similar to for example Ubuntu PPAs (Personal Package Archives).

The AUR offers a large number of packages, made installable by a specific script format known as PKGBUILD.

In most cases, one can opt to use an AUR helper, such as `yay` or `paru`, to seamlessly install software from official
sources alongside the AUR without much extra work.

Recently, I wanted to adjust an already existing AUR package, `evdi-git`, which is one half of a package bundle to make
use of USB docking stations (think HDMI/USB ports/network) published by DisplayLink. (the other half is `displaylink` in
case you wondered). This package is exceptionally unreliable, and this case, it is not yet supporting Linux kernel 5.15.

But! Lo and behold, someone had forked the official repo (thanks [francoism90](https://github.com/francoism90)!) and
published a fix which was confirmed by a few others as commented in the AUR.

Now, but how to install this fork? Below is what I did.

To access the AUR files, use `paru` and with the help of the -G parameter, download the PKGBUILD: `paru -G evdi-git`
The long form of this parameter is `--getpkgbuild`.

Then, open the downloaded folder/file with your favourite text editor, and adjust two keys: `url` and `source`.
The first one is not strictly needed, but can be included for correctness.

Once adjusted, build and install based on the adjustments: `paru -Ui`.
The long name is `--upgrade` followed by `--install`.

Below is a diff of the PKGBUILD changes:

```patch
--- a/PKGBUILD
+++ b/PKGBUILD
@@ -4,12 +4,12 @@
pkgname='evdi-git'
pkgver=1.9.1.r4.gb0b3d13
_pkgver="${pkgver%%.r*}"
-url='https://github.com/DisplayLink/evdi'
+url='https://github.com/francoism90/evdi'
source=(
-  'git+https://github.com/DisplayLink/evdi'
+  'git+https://github.com/francoism90/evdi'
   )
```

That was all what I needed to do to allow me using my external monitor once again.

One probably should take a moment to increment the package version used too - I manually uninstalled the old version
first though as a quick solution.
