---
title : "Hacker School"
date : 2020-06-15T22:34:44+02:00
image : "/images/hacker-school-logo.png"
alt : "Hacker School"
color : ""
link1 : "https://hacker-school.de/"
link2 : ""
projectDate : "August 2019 - ongoing"
draft : false
description: "My 'Hacker School' project experience"
---

### Summary

The Hacker School is a German organisation aiming to teach children and youth programming and software developing
using a more interactive project-based approach.

The organisation is a NGO based in Hamburg, with courses all over Germany.

My participation have been focused around workshops using the [micro:bit](https://en.wikipedia.org/wiki/Micro_Bit)
platform, which aims to teach programming concepts and logic through a drag and drop GUI.

Despite its simplicity, the micro:bit allows for an impressive creativity and freedom.

### Tools

* MakeCode: Web IDE
* Patience: Kids have a different (shorter) attention span
