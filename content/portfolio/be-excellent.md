---
title : "BE EXCELLENT (now Dept)"
date : 2020-06-12T20:59:29+02:00
image : "/images/be-excellent-logo.png"
alt : "CRM and E-Commerce solutions"
color : ""
link1 : "https://www.deptagency.com/en-gb/service/360-commerce/"
link2 : ""
projectDate : "September 2015 - March 2018"
draft : false
description: "My 'BE EXCELLENT' work experience"
---

### Summary

* I was a founding member of a new and exploratory CRM-focused branch within the company, which was a specialized
consulting and services business.
* My areas of responsibility were split between implementation, project management and customer communication.
* Implementation areas were covering web-based frontends, mobile development or enterprise system configuration and setup.

### Tasks

#### Oracle CRM solutions

Setting up and customising systems such as Sales Cloud, Service Cloud (RightNow) as well as Commerce Cloud and making
use of cloud tools such as Integration Cloud.

#### Mobile cross-platform development

Worked with a relatively young e-commerce tablet application (Demandware Endless Aisle).
Customizations such as country-specific features were developed in discussion with customer requirements, as were
layouts and designs. Also backend development came into place, extending Demandware’s OCAPI.

#### Web frontend development

A re-design and re-implementation of the web frontend of an Oracle Service Cloud system,
putting in place a mobile-first approach.

#### Consulting

Experience with external as well as internal consulting, accompanied by project planning, requirement gathering and
analysing allowed me to gain a very all-round consultancy skillset.

### Tools

Technical skills acquired ranges from CSS/HTML/JavaScript (jQuery)/PHP web frontend technologies,
the Appcelerator Titanium JavaScript framework, Demandware frontend/backend (both Controller and Pipeline-based)
to Java Groovy scripting.
