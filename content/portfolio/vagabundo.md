---
title : "Vagabundo"
date : 2020-06-12T21:53:35+02:00
image : "/images/vagabundo-logo.svg"
alt : "Senacor Technologies"
color : "#004953"
link1 : "http://vagabundo.green/start"
link2 : ""
projectDate : "January 2019 - February 2019"
draft : false
description: "My 'Vagabundo' project experience"
---

### Summary

A Senacor internal project to evaluate the maturity of serverless (or function-as-a-service).
I was working with a small team to calculate and visualise the CO₂ emissions caused by the travelling of each employee.
This could then be compensated by CO₂ offsetting.

The taxi, flight and train data was gathered for all (500+) employees, evaluated
and visualised through different dashboards.

Note that this information is not public, and can only be accessed as a Senacor employee.

### Tools

* AWS: Serverless functions
* Angular: Frontend framework
* D3: Data visualization library
* MySQL: Persistence
