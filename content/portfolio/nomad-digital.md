---
title : "Nomad Digital"
date : 2020-06-12T21:18:27+02:00
image : "/images/nomad-logo-white.svg"
alt : "System administration"
color : "#E31B23"
link1 : "https://nomad-digital.com/"
link2 : ""
projectDate : "August 2013 - July 2014"
draft : false
description: "My 'Nomad Digital' work experience"
---

### Summary

As part of my Computing Science degree I spent a year in industry at Nomad Digital,
where I was working at the System Administration team.

The work focused on maintaining the reliable operation and upkeep of the systems and network utilised within
the company with an emphasis on Linux environments, Virtualisation and Cisco networking equipment.

Skills acquired ranges from

* Experience with various monitoring software
* Troubleshooting and error diagnosis for both software and hardware
* Computer network administration
* Documentation of tasks and procedures
* Backups and their implementations

Technical Skills

* Linux
* Virtualisation - Citrix XenServer
* Server Hardware
* Networking - Cisco
