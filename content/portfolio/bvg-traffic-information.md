---
title : "Berliner Verkehrsbetriebe"
date : 2020-06-15T23:23:20+02:00
image : "/images/BVG_Herz_Gelb.svg"
alt : "BVG Traffic Information"
color : ""
link1 : "https://olof-nord.gitlab.io/bvg-traffic-information/"
link2 : "https://gitlab.com/olof-nord/bvg-traffic-information"
projectDate : "April 2020 - May 2020"
draft : false
description: "My 'BVG Traffic Information' project"
---

### Summary

A traffic information website to present bus, tram, subway and ferry public transport status information in Berlin.

The project is a redesign of the [BVG Verkehrsmeldungen](https://www.bvg.de/de/Fahrinfo/Verkehrsmeldungen) section of
the BVG website.

The project can be accessed by this [link](https://olof-nord.gitlab.io/bvg-traffic-information/).

### Tools

* Angular 9 for its web framework.
* Node 12 for its JavaScript runtime.
* ngrx for its state management
