---
title : "Teambank"
date : 2020-06-15T23:11:33+02:00
image : "/images/easycredit-logo.svg"
alt : "Senacor Technologies"
color : ""
link1 : "https://www.teambank.de/"
link2 : ""
projectDate : "April 2018 - December 2018"
draft : false
description: "My 'Teambank' project experience"
---

### Summary

An ongoing Senacor project, which aims to expand the digital product portfolio offered by the bank.

### Tools

* Angular 6: Frontend framework
* Bootstrap: Webdesign framework
