---
title: About
date: 2020-06-05T22:30:09+02:00
draft: false
---

Hi, I'm Olof Nord.

I am a software engineer with a passion for software and tech.

I like to believe I am enthusiastic, positive and motivated, and always curious to learn more.

I like people who make me think.
