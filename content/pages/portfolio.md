---
title: Portfolio
date: 2020-10-31T15:29:02+01:00
draft: false
---

## Projects I have participated in

Below is an incomplete list of projects and organisations which I have been involved with.

The order is roughly by time: the most recent first.

### Projects/Organisations

- [BVG Traffic Information](/portfolio/bvg-traffic-information/)
- [Hacker School](/portfolio/hacker-school/)
- [Vagabundo](/portfolio/vagabundo/)

### Companies

- [Teambank](/portfolio/teambank/)
- [Be Excellent](/portfolio/be-excellent/)
- [Nomad Digital](/portfolio/nomad-digital/)
