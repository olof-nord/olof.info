# Olof Nord's Blog

[![Netlify Status](https://api.netlify.com/api/v1/badges/e7185818-c8f5-446b-b51b-8c0321512e32/deploy-status)](https://app.netlify.com/sites/olof/deploys)

My personal portfolio website and blog

## Technologies used

- [Hugo](https://gohugo.io/) for its static page generator
- [hugo-ficurinia](https://gitlab.com/gabmus/hugo-ficurinia) for its hugo theme
- [Netlify](https://www.netlify.com/) for web project management
- [Plausible](https://plausible.io/) for web analytics
- [Commento](https://commento.io) for comments
- [Feathericons](https://feathericons.com//) for logo and favicon

## License

>You can check out the full license [here](https://github.com/olof-nord/olof.info/blob/master/LICENSE)

This project is licensed under the terms of the **AGPL-3.0** license.
